// Functions
// Parameters and Arguments
// Functions in javascript are lines/blocks of codes that tell our device/ application to perform certain tasks When they are called.
// Functions are mostly created to create complicated tasks to run several lines of code in succession
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

function printInput() {
    let nickname = prompt("Enter your nickname: ");
    console.log("Hi, " + nickname);
};

printInput();

// For other cases, functions can also process data directly passed into it instead of relying on global variables and prompt()

function printName(name) {
    console.log("My name is " + name);
};

printName("Juana");
printName("Xandrix");

// You can directly pass data into the function. The function can then call/ use that data which is referred as "name"

// "name" is a parameter
// A "parameter" acts as a named variable/container that exists only inside of a function
// It is used to store information that is provided to a function when it is called/ invoked

// Variable can also be passed as an argument
let sampleVariable = "Yui";
printName(sampleVariable);

// function arguments cannot be used by a function if there are no parameters provided within the function

function checkDivisibilityBy8(num) {

    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);

    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?")
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments
// Function parameters can also accept other functions as arguments
// Some complex functions use other functions as arguments to perform more complicated results

function argumentFunction() {
    console.log("This function was passed as an argument before the message was printed");
};

function invokeFunction(argumentFunction) {
    argumentFunction();
};

// Adding and removing parentheses "()" impacts the output of JavaScript heavily
// When a function is used with parentheses it denotes invoking/ calling a function
// A function used without a parentheses is normally associated with using the function as an argument to another funtion.

invokeFunction(argumentFunction);

// Finding information about a function in the console.log()
console.log(argumentFunction);

// Using multiple parameters
// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

function createFullName(firstname, middlename, lastname) {
    console.log(firstname + " " + middlename + " " + lastname);
};

createFullName('Juan', 'Dela', 'Cruz');

// In JavaScript, providing more/less arguments than the expected parameters will not return an error.

// Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.

createFullName('Juan', 'Dela');
createFullName('Juan', 'Dela', 'Cruz', 'Hello');

// Using variables as arguments

let firstname = 'John';
let middlename = 'Doe';
let lastname = 'Smith';

createFullName(firstname, middlename, lastname);

// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be recieved in the same order it was passed.

// The return statement
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstname, middlename, lastname) {

    return firstname + ' ' + middlename + ' ' + lastname

    console.log("This message will not be printed");
};

let completeName = returnFullName("Jeffery", "Amazon", "Bezos");
console.log(completeName);

// Notice that the console. log() after the retrun is no longer printed in the console. That is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution

// This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console

// In this example, console.log() will print the returned value of the returnFu11Name() function
 
 
console.log(returnFullName(firstname, middlename, lastname));

// MINI ACTIVITY
// Create a function that will add 2 numbers together
// You must use parameters and arguments

function addNum (num1, num2) {
    sum = num1 + num2
    console.log("The sum is: " + sum);
};

addNum(5, 6);

// You can also create a variable inside the function to contain the result and return that variable instead.

function returnAddress(city, country) {
    let fullAddress = city + ', ' + country;
    return fullAddress;
};

let myAddress = returnAddress("Cebu City", "Cebu");
console.log(myAddress);

// On the other hand, when a function only has console.log() to display its results it will return undefined instead.

function printPlayerInfo(username, level, job) {
    console.log("Username: " + username);
    console.log("Level: " + level);
    console.log("Job: " + job);
};

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);

// Returns undefined because printP1ayerInfo returns nothing. It only console. logs the details
// You cannot save any value from printP1a erlnfo() because it does not return anything
 

 















































































