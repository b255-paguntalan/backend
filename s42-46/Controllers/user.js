const bcrypt = require("bcrypt");
const auth = require("../auth");
const Users = require("../models/Users");
const Product = require("../models/Products");


module.exports.registerUser = (reqBody) =>{
	let newUser = new Users({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	return Users.find({email : reqBody.email}).then(result => {
		if(result.length > 0) {
			return `Email already exists`
		}
		else {
			return newUser.save().then((user, error) =>{

				if(error) {
					return "Failed to register user";
				} else {
					return Users.find({email : reqBody.email}).then(result => {
						return `Successfully registered user!
						
						Email: ${result[0].email}`
					})
				};
			})
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return Users.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return `No user found, please register first.`;

		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				
				return Users.find({email : reqBody.email}).then(result => {
					return `Login successful!
					
					User ID: ${result[0]._id}`
				})
				

			} else {
				return false;
			}
		}
	})
}

module.exports.loginAdmin = (reqBody) => {

	return Users.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return `No user found, please register first.`;

		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return Users.find({email : reqBody.email}).then(result => {
					return `Admin login successful!
					
					Admin ID: ${result[0]._id}
					
					Admin access token: ${auth.createAccessToken(result)}`

				})

			} else {
				return false;
			}
		}
	})
}

module.exports.setAdmin = (reqParams, reqBody) => {

	let updateUser = {
		isAdmin : reqBody.isAdmin
	}

	return Users.findByIdAndUpdate(reqParams.userId, updateUser).then((user, error) =>{

		if(error){
			return `Failed to update product: ${productId}`;

		}
		else{
			return Users.findById(reqParams.userId).then(result => {
				return result
			})
		}
	})
}

module.exports.buy = async (data) => {

	let product = await Product.findById(data.productId);

	if(!product || product == null) {
		return `Product not found ${data.productId}`
	}
		
	let isUserUpdated = await Users.findById(data.userId).then(user =>{
		
		user.orderedProduct.push({
			products: [{
				productId: data.productId,
				productName: product.productName,
				quantity: data.quantity
			}],
			totalAmount: data.quantity * product.price
		});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product =>{
		
		product.userOrders.push({userId : data.userId});


		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	
	if(isUserUpdated && isProductUpdated){
		return `Successfully created an order.`

	} else {
		return false
	}
};

module.exports.getProfile = (data) => {

	return Users.findById(data.userId).then(result => {

		if(result == null){
			return false
		} else {
			result.password = "******";

			return result;
		}

	});

};
