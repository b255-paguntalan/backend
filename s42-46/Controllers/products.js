const Product = require("../models/Products");

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		productName : reqBody.productName,
		description : reqBody.description,
		price : reqBody.price
	});

	return newProduct.save().then((products, error) => {

		if(error) {
			return `Error adding a product`

		} else {
			return `Successfully added a product! 

			Product Name: ${reqBody.productName} 
			Product description: ${reqBody.description}
			Price: ${reqBody.price}`;
		}
	})
}

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		productName : reqBody.productName,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((products, error) =>{

		if(error){
			return `Failed to update product: ${productId}`;

		}else{
			return `Successfully updated product to:

			Product Name: ${reqBody.productName} 
			Product description: ${reqBody.description}
			Price: ${reqBody.price}`;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((products, error) => {

		if (error) {

			return false;

		} else {

			return `Archive successful`;

		}

	});
};

module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((products, error) => {

		if (error) {

			return false;

		} else {

			return `Activate successful`;

		}

	});
};