const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/users");
const productRoutes = require("./routes/products");

const app = express();

mongoose.connect("mongodb+srv://xandrixjill-255:admin123@zuitt-bootcamp.qbuqjn8.mongodb.net/?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    UseUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Successfully connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.use("/users", userRoutes);
app.use("/products", productRoutes)

if(require.main === module) {
    app.listen(process.env.PORT || 4000, () => {
        console.log(`API is now online on port ${ process.env.PORT || 4000}`)
    });
}

module.exports = app;