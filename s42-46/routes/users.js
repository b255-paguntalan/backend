const express = require("express");
const router = express.Router();
const userController = require("../Controllers/user");
const auth = require("../auth");


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/loginAdmin", (req, res) => {
    userController.loginAdmin(req.body).then(resultFromController => res.send(resultFromController))
});

router.put("/:userId", auth.verify, (req, res) => {
	userController.setAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/buy",(req, res) => {
    let data = {
        userId: req.body.userId,
        productId: req.body.productId,
        quantity: req.body.quantity
    }

    userController.buy(data).then(resultFromController => res.send(resultFromController));
})

router.get("/details",(req, res) => {

	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});

module.exports = router;