const express = require("express");
const router = express.Router();
const productController = require("../Controllers/products");
const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
	productController.addProduct(req.body).then(resultFromController =>
	res.send(resultFromController))
});

router.get("/allProducts", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
});

router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/activate", auth.verify, (req, res) => {
	productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;