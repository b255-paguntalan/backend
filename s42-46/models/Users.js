const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    email : {
        type : String,
        required : [true, "Email is required"]
    },

    password : {
        type : String,
        required : [true, "Password is required"]
    },

    isAdmin : {
        type : Boolean,
        default : false
    },

    orderedProduct: [{

        products: [{

            productId: {

            },

            productName: {
                type: String
            },

            quantity: {
                type: Number
            }

        }],

        totalAmount: {
            type: Number
        },

        purchasedOn: {
            type: Date,
            default: Date.now
        }

    }]
        
})

module.exports = mongoose.model("Users", userSchema);