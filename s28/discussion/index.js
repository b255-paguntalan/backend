// CRUD operations

// Insert document (CREATE)

/* 

    Syntax: 
        Insert One Document
            db.collectionName.insertOne({
                "fieldA": "valueA",
                "fieldB": "valueB",
            });
        
        Insert Many Documents
            db.collectionName.insertMany([
                {
                    "fieldA": "valueA",
                    "fieldB": "valueB",
                },

                {
                    "fieldA": "valueA",
                    "fieldB": "valueB",
                }
            ])
*/

db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Doe",
    "age": 21,
    "email": "janedoe@mail.com",
    "company": "none"
});

db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "company": "none"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "company": "none"
    }
]);

// Find Documents(Read/Retrive)

// This will retrieve all the documents in the collection

db.users.find();  

db.users.find({"firstName": "John"});

db.users.find({
    "firstName": "Neil",
    "lastName": "Armstrong",
    "age": 82
});

// Returns the first document in our collection

db.users.findOne({
    "firstName": "Stephen"
})

// Update documents

db.users.insertOne({
        "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "email": "test@mail.com",
        "company": "none"
});

// Updating one document
db.users.updateOne(
    {
        "firstName": "Test"
    },

    {
        $set:{
            "firstName": "Bill",
            "lastName": "Gates",
            "age": 65,
            "email": "billgates@mail.com",
            "department": "Operations",
            "status": "active"
        }
    }
);

// Removing a field

db.users.updateOne(
    {
        "firstName": "Bill"
    },

    {
        $unset:{
            "status": "active"
        }
    }
)

// Updating multiple documents

db.users.updateMany(
    {
        "company": "none"
    },

    {
        $set: {
            "company": "HR"
        }
    }
);

db.users.updateMany(
    {},

    {
        $set:{
            "company": "comp"
        }
    }
)

// Deleting documents

db.users.insertOne({
    "firstName": "Test"
})

db.users.deleteOne({
    "firstName": "Test"
})

db.users.deleteMany({
    "company": "comp"
})

db.courses.deleteMany({})