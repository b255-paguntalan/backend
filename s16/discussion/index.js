// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators 

    // Basic assignment operator (=)
    let assignmentNumber = 8;

    // Addition assignment operator (+=)
    // The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

    assignmentNumber = assignmentNumber + 2;
    console.log("Result of addition assignment operator: " + assignmentNumber);

    // Shorthand
    assignmentNumber += 2;
    console.log("Result of addition assignment operator: " + assignmentNumber);

    assignmentNumber -= 2;
    console.log("Result of addition subtraction operator: " + assignmentNumber);

    assignmentNumber *= 2;
    console.log("Result of addition multiplication operator: " + assignmentNumber);

    assignmentNumber /= 2;
    console.log("Result of addition division operator: " + assignmentNumber);


// Multiple Operators and Parenthesis

    /* 
    
    -   When multiple operators are applied in a single statement, it follows the PEMDAS
        (Parentheses, Exponents, Multiplication and Division (from left to right), Addition and Subtraction (from left to right).)

    */

    let mdas = 1 + 3 - 3 * 4 / 5;
    console.log("Result of mdas operators: " + mdas);

    let pemdas = 1 + (2 - 3) * (4 / 5);
    console.log("Result of pemdas operators: " + pemdas);


//  Increment and Decrement
    let z = 1;

    let increment = ++z;
    console.log("Result of increment: " + increment);

    let decrement = --z;
    console.log("Result of decrement: " + decrement);


// Type coertion

    /* 
        - Type coertion is the automatic or implicit convertion of values from one data type to another
        - This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
        - Values are automatically converted from one data type to another in order to resolve operations

    */

        let numA = '10';
        let numB = 12;

        // Adding/concatinating a string and a number will result in a string

        let coertion = numA + numB;
        console.log(coertion);
        console.log(typeof coertion);

        let numE = true + 1;
        console.log(numE);

        let numF = false + 1;
        console.log(numF);


// Compasison Operators 

    let juan = 'juan';

    // Equally Operator (==)

    /* 
        - Checks whether the operands are equal/have the same content
        - Returns a boolean value

    */

    console.log(1==1);
    console.log(1==2);
    console.log(1=='1');
    console.log(0==false);

    // Compares two strings that are the same

    console.log('juan' == 'juan');

    // Comapres a string with the variable "juan" declared above

    console.log('juan' == juan);


// Inequality Operator 

    /* 
        - Checks whether the operands are not equal/have different content.
    */

    console.log("Start of inequality operators")
    console.log(1 != 1);
    console.log(1 != 2);
    console.log(1 != '1');
    console.log(0 != false);
    console.log('juan' != 'juan');
    console.log('juan' != juan);

    // Strict Equality Operator

    /* 
        - Checks whether the operants are equal/have the same contents
        -Also COMPARES the data types of 2 values
        - JavaScript is a loosely typed language meaning that alues of different data types can be stored in variables
        - This sometimes can cause problems problems within our code
    */

    console.log("Start of Strict Equality Operator")

    console.log(1 === 1);
    console.log(1 === 2);
    console.log(1 === '1');
    console.log(0 === false);
    console.log('juan' === 'juan');
    console.log('juan' === juan);

    // Strict Inequality Operator

    console.log("Start of Strict Inequality Operator")

    console.log(1 !== 1);
    console.log(1 !== 2);
    console.log(1 !== '1');
    console.log(0 !== false);
    console.log('juan' !== 'juan');
    console.log('juan' !== juan);


// Relational Operators 

    // Some comparison operations check whether one value is greater or less than to the other value

    let a = 50;
    let b = 65;

    // GT or Greater Than operator (>)
    let isGreaterThan = a > b;

    // LT or less than Than operator (<)
    let isLessThan = a < b;

    // GTE or Greater Than or Equal to (>=)
    let isGTorEqual = a >= b;

    // GTE or Greater Than or Equal to (<=)
    let isLTorEqual = a <= b;   

    console.log("Start of relational operators");
    console.log(isGreaterThan);
    console.log(isLessThan);
    console.log(isGTorEqual);
    console.log(isLTorEqual);


// Logical Operators 

    let islegalAge = true;
    let isRegistered = false;

    // Logical AND Operator (&& - Double Ampersand)
    // Returns true if all operands are true 

    let allRequirementsMet = islegalAge && isRegistered;
    console.log("Result of Logical AND operator: " + allRequirementsMet);

    // Logical OR Operator (|| - Double Pipe)
    // Returns true if one of the operands are true 

    let someRequirementsMet = islegalAge || isRegistered;
    console.log("Result of Logical OR operator: " + someRequirementsMet);

    // Logical NOT Operator (! - Exclamation Point)
    // Returns the opposite value

    let someRequirementsNotMet = !isRegistered;
    console.log("Result of Logical NOT operator: " + someRequirementsNotMet);



















