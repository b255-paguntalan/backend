// [SECTION] While loop

/* 

    - A while loop takes in an expression/condition
    -Expression are any unit of code that can be evaluated to a value
    - If the condition evaluates to true, the statements inside the code block will be executed
    - A statement is a command the programmer gives to the computer 
    - A loop will iterate a certain number of times until an expression/condition is ment


*/

let count = 5;

while (count !== 0) {
    console.log("While: " + count);
    count--;
};

console.log("--------------------------------------")

// [SECTION] do-while loop

/* 

    - A do-while loop works a lot like the while loop.
    But unlike the while loops, do-while loops guarantee that the code will be executed at least once.

*/


 let number = parseInt(prompt("Give me a number:"))

do {
    console.log("Do-while: " + number);
    
    number+=1;
}
while (number < 10);  

console.log("--------------------------------------")


// [SECTION] For loops

/* 

    - A for loop is more flexible that while and do-while loops.
    1. The initialization value that will track the progression of the loop.
    2. The expression/condition that will be evaluated which will determine whether the loop will run one more time.
    3. The finalExpression indicates how to advance the loop

*/

for (let count=0; count <= 20; count++){
    console.log(count);
}

let myString = "Alex";
console.log(myString.length)

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for(let x = 0; x < myString.length; x++) {
    console.log(myString[x]);
};

console.log("--------------------------------------")

// MINI-ACTIVITY START
// 1. Write a for loop that will print numbers from 1-10
// 2. It must be inside a function

function miniActivity(num) {
    for(let num=1; num <= 10; num++){
        console.log(num);
    };
};

miniActivity();

// MINI-ACTIVITY END

console.log("--------------------------------------")

let myName = "ALeX";

for(let letter=0; letter<myName.length; letter++) {
    // console.log(myName[letter].toLowerCase());

    if(
        myName[letter].toLowerCase() == "a" || 
        myName[letter].toLowerCase() == "e" ||
        myName[letter].toLowerCase() == "i" ||
        myName[letter].toLowerCase() == "o" ||
        myName[letter].toLowerCase() == "u" 
    ){
        console.log(3);
    }
    else {
        console.log(myName[letter]);
    };
};

console.log("--------------------------------------")

// [SECTION] Continue and Break statements

/* 

    - The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements
    - The break statements is used to terminate the current loop once a match has been found

*/

for ( let count=0; count<=20;count++) {
    // If remainder is equal to 0
    if(count % 2 === 0) {
        // Tells the code to continue to the next iteration of the loop
        // This ignores all statements located after the continue statement
        continue;
    }

    console.log("Continue and break: " + count)

    if (count > 10) {
        break;
    };
};

console.log("--------------------------------------")

let name = "alexandro"

for (let letter=0; letter<name.length; letter++) {
    console.log(name[letter])

    if(name[letter].toLowerCase() === 'a') {
        console.log("Continue to the next iteration");
        continue;
    }

    if(name[letter] == 'd') {
        break;
    };
};