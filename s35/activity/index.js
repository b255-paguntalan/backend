const express = require("express");
const mongoose = require("mongoose");
const activity = express();
const port = 3001;

mongoose.connect("mongodb+srv://xandrixjill-255:admin123@zuitt-bootcamp.qbuqjn8.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

const userSchema = new mongoose.Schema({
    userName: String,
    password: String
})

const User = mongoose.model("User", userSchema);

activity.use(express.json());
activity.use(express.urlencoded({extended:true}));
activity.post("/signup", (req, res) => {
    User.findOne({userName: req.body.userName}).then((result, err) => {
        if(result != null && result.userName == req.body.userName){
			return res.send("Duplicate username found");
        }
        else {
            if(req.body.userName != null && req.body.password != null) {
                let newUser = new User({
                    userName: req.body.userName,
                    password: req.body.password
                });

                newUser.save().then((savedUser, saveErr) => {
                    if(saveErr) {
                        return console.error(saveErr);
                    }
                    else {
                        return res.status(201).send("New user registered")
                    }
                })
            }
            else {
                return res.send("BOTH username and password must be provided")
            }

            
        }
    })
})

activity.get("/getUsers", (req, res) => {
    User.find({}).then((result, err) => {
        if(err) {
            return console.log(err)
        }
        else {
            return res.status(200).json({
                data:result
            })
        }
    })
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))

if(require.main === module){
	activity.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = activity;