// Use the "require" directive to load Node.js module
// A "module" is a software compenent or part of a program that contains one or more routines
// The "HTTP module" lets Node.js transfer data using the Hyper text transfer protocol
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients(browser) and servers(nodejs/express js applications) communicate by exchanging individual messages
// The messegas sent by the client, usually a web browser, are called requests 
// the messages sent by the serve as an answer are called responses

let http = require("http");

// Using this module's createserver() methods we can create an HTTP server that listens to requests on a specified port and gives reponses
// the http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are requests and response objects(data type) that contains methods that 1 lows us to receive requests from the clien and send responses back

http.createServer(function(request, response){

    // The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
    // The method "GET" means that we will be retrieving or reading information
    if(request.url == "/items" && request.method == "GET"){

        // Requests the "?items" path and "GETS" information
        response.writeHead(200, {"Content-Type": "text/plain"});
        // End the response process
        response.end("Data retrieved from the database");
    }

    // The method "POST" means that we will be adding or creating information
    // In this example, we will just be sending a text response for now
    if(request.url == "/items" && request.method == "POST"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Data to be sent to the database");
    }
}).listen(4000);

console.log("Server is running at localhost:4000")