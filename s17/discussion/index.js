// Functions 
    // Functions in JavaScript  are lines/blocks of codes that tell our device/application to perform a certain task
    // Functions are mostly created to create complicated tasks to run several lines of code in succession

// Function declaration

    function printName() {
        console.log("My name is John");
    };

// Function Invocation

    printName();

// Function declarations vs expressions

    // Function declaration

    // A function can be created thought function declaration by using the function keyword and adding a fuction name

    declaredFunction();

    function declaredFunction() {
        console.log("Hello world from declaredFunction()");
    };

    declaredFunction();

    // Function expression
    // A function expression can also be stored in a variable. This is called function expression

    // A function expression is an anonymous function assigned to the variableFunction

    // Anonymous function - a function without a name

    let variableFunction = function() {
        console.log("Hello again!");
    };

    variableFunction();

    // We can also create a function expression of a named function

    // However, to invoke the function expressiom, we invoke it by its variable name, not by its function name

    let funcExpression = function funcName() {
        console.log("Hello from the other side");
    };

    funcExpression();

    // You can reassign declared functions and function expressions to new anonymous functions

    declaredFunction = function() {
        console.log("Updated declaredFunction");
    };

    declaredFunction();

    funcExpression = function() {
        console.log("Updated funcExpression");
    };

    funcExpression();

    // Howevre we cannot re-assign a function expression initialized with const

    const constantFunc = function() {
        console.log("Initialized with const!");
    };

    constantFunc();

//  Function scoping

/* 

    scope is the accessibility (visibility) of variables
    within our program

    JavaScript Variables has 3 types of scope:
    1. local/ block scope
    2. global scope
    3. function scope

*/

    {
        let localVar = "Armando Perez";
    }

    let globalVar = "Mr. Worldwide";

    console.log(globalVar);

    // Function scope

    function showNames() {
        var functionVar = "Joe";
        const functionConst = "John";
        let functionLet = "Jane";

        console.log(functionVar);
        console.log(functionConst);
        console.log(functionLet);
    }

    showNames();

    // Nested functions

        // You can create another function inside a function. This is called a nested function.

        function myNewFunction() {
            let name = "Jane";

            function nestedFunction() {
                let nestedName = "John";

                console.log(name)
            }

            nestedFunction();

        }

        myNewFunction();

        // Function and global Scoped Variables
            // Global Scoped variables

            let globalName = "Alexandro";

            function myNewFunction2() {
                let nameInside = "Renz";

                console.log(globalName);
            };

            myNewFunction2();

            // Using alert()

                // Alert allows us to show a small window at hte top of our browser page to show information to our users. As opposed to console.log()

                alert("Hello world!");
            
                function showSampleAlert() {
                    alert("Hello, User");
                };

                showSampleAlert();

                console.log("I will only show in the console when the alert is dismissed");

            
            // Using prompt()
            // prompt() allows us to show a small window at the browser to gather a small user input. It, much like alert() will have the wait until it is dismissed

            let samplePrompt = prompt("Enter your name: ");
            console.log("Hello, " + samplePrompt);

            let sampleNullPrompt = prompt("dont enter anything.");

            console.log(sampleNullPrompt);
                // Returns an empty string when there is no input. Or null if the user cancels the prompt.

            function printWelcomeMessage() {
                let firstName = prompt("Enter your first name: ");

                let lastName = prompt("Enter your last name: ");

                console.log("Hello, " + firstName + " " + lastName + "!");
                console.log("Welcome to my page!");
            };

            printWelcomeMessage();

            // Function Naming Conventions
    // Function names should be definitive of the task It will perform. It usually contains a verb,

    function getCourses() {
        let courses = ["Science 101", "Math 101", "English 101"];
        console.log(courses);

    };

    getCourses();

    function displayCarInfo() {
        console.log("Brand: Toyota");
        console.log("Type: Sedan");
        console.log("Price: 1,500,000");
    }

    displayCarInfo();









































