const express = require("express");
const activity = express();
const port = 3000;
activity.use(express.json());
activity.use(express.urlencoded({extended:true}));

if(require.main === module){
    activity.listen(port, () => console.log(`Server running at port ${port}`))
}

users = [{"userName": "johndoe", "password": "johndoe1234"}]

// Number 1
activity.get("/home", (req, res) => {
    res.send("Welcome to the home page")
})

// Number 3
activity.get("/users", (req, res) => {
    res.send(users)
})

// Number 5-6
activity.delete("/delete-user", (req, res) => {
    let message;

    if(users.length>0) {
        for(let i=0; i<users.length; i++){
            if(req.body.userName === users[i].userName){
                users.splice(i, 1)
                message = `User ${req.body.userName} has been deleted.`
                break;
            }
        }
        if(!message) {
            message = "User does not exist.";
        }
    }
    else {
        message = "No user found."
    }

    res.send(message);
})