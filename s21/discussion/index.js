// An array in programming is simply a list of data. Let's write the example earlier

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// [SECTION] Arrays

/* 

    -Arrays are used to store multiple related values in a
    single variable
    -They are declared using square brackets() also known
    as "Array Literals"
    -Commonly used to store numerous amounts of data to
    manipulate in order to perform a number' Of tasks

*/

// Common examples for arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of an array but it not recommended 
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays
let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
]

// Creating an array with values from variables
let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// [SECTION] length property

// The .length property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr);

// length property can also be used with strings. some arrays and properties can also be used with strings.
let fullname = 'Jamie Noble'
console.log(fullname.length);

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// To delete  a specific item in an array we can emplyo array methods(which will be shown in the next session)

// Another example of using decrement
cities.length--;
console.log(cities);

fullname.length = fullname.length-1;
console.log(fullname.length);
fullname.length--;
console.log(fullname);

// If you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property
 
let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++;
console.log(theBeatles);

// [SECTION] Reading from arrays

/* 

    -Accessing array elements is one of the more common
    tasks that we do with an array
    -This can be done through the use of array indexes
    -Each element in an array is associated with it's own
    index/number
    -In javascript, the first element is associated with
    the number and increasing this number by 1 for
    every proceeding element
    •The reason an array starts with e is due to how the
    language is designed
    -Array indexes actually refer to an address/ location
    in the devices memory and how the information is
    stored
    - Example array location in memory
        Array[0] = eX7ffe947bade
        Array[1] = OX7ffe947bad4
        Array[2] = ex7ffe947bad8

*/

console.log(grades[0]);
console.log(computerBrands[0]);
// Accessing an array element that does not exist will return "undefined"
console.log(grades[20]);

let lakersLegend = ['Kobe', 'Shaq', 'LeBron', 'Magic', 'Kareem'];

console.log(lakersLegend[1]);
console.log(lakersLegend[3]);

// You can save or store array items in another variable
let currentLaker = lakersLegend[2];
console.log(currentLaker);

// you can also reassign array values using the item's indices
console.log("Array before reassignment");
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegend);

// Accessing the last element of an array
// Since the first element Of an array starts with B, subtracting 1 to the length of an array will offset the value by one allowing us to the get the last index

let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);

// You can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items into the array

let newArr = [];
console.log(newArr[0]);
newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// You can also add items at the end of the array.
newArr[newArr.length] = "Aerith Gainsbrough";
console.log(newArr)

// Looping over an array
// You can use a for loop to iterate all items in an array
// Set the counter as the index and set a condition that as long as the current index iterated is less that the 

for(let index = 0; index<newArr.length; index++) {
    console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++) {
    if(numArr [index] % 5 === 0) {
        console.log(numArr[index] + " is divisible by 5");
    }
    else {
        console.log(numArr[index] + " is not divisible by 5");
    }
}

// [SECTION] Multidimentional Array

/* 

    -Multidimensional arrays are useful for storing
    complex data structures
    -A practical application of this sis to help visualize
    /create real world objects
    -Though useful in a number of cases, creatine complex
    array structures is not always recommended

*/

let chessBoard = [
    ['al', 'bl', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
]

console.log(chessBoard);

// Accessing elements of a multidimentional array
console.log(chessBoard[1][4]);

console.log("Pawn moves to: " + chessBoard[1][5]);
