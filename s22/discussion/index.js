// Array Methods

// JavaScript has built in functions and methods for arrays. This allows s to manipulate and access array items.

// Mutator Methods

/* 

    - Mutators Methods are functions that "mutate" or change an array after they're created
    - These methods manipulate the original array performing various tasks such as adding and removing elements.

*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];


console.log('-----------------------------------------------------------------------');


// push()

/* 

    - Adds an element in the end of an array AND returns the array's length

*/

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method');
console.log(fruits);


console.log('-----------------------------------------------------------------------');


// Adding multiple elements to an array

fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method');
console.log(fruits);


console.log('-----------------------------------------------------------------------');


// pop()

/* 

    - Removes the last element of an array and returns the removed element

*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method');
console.log(fruits);



console.log('-----------------------------------------------------------------------');



// unshift()

/* 

    - Adds an element at the beginning of an array

*/

fruits.unshift('Lime', 'Banana');
console.log(fruits);


console.log('-----------------------------------------------------------------------');


// shift()

/* 

    -Removes an element at the beginning of an array AND
    returns the removed element

*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);


console.log('-----------------------------------------------------------------------');

// splice()

/* 

    - Simultaneously removes elements from specified index number and adds elements

*/

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method')
console.log(fruits);


console.log('-----------------------------------------------------------------------');


// sort()

/* 

    - Rearanges the array elements in alphanumeric order

*/

fruits.sort();
console.log('Mutated array from sort method')
console.log(fruits);

/* 

    Important Note:
    - The "sort" method is used for more complicated sorting functions

*/


console.log('-----------------------------------------------------------------------');


// reverse()

/* 

    - Reverses the order of array elements

*/

fruits.reverse();
console.log('Mutated array from reverse method')
console.log(fruits);


console.log('-----------------------------------------------------------------------');


// Non-mutator methods

/* 

    - Non-mutator methods are functions that do not modify or change an array after they're created
    - These methods do not manipulate original array performing various tasks such as returning elements from an array and combining array

*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()

/* 

    - Returns the index number of the first matching element found in an array
    - If no match was found, the result will be -1
    - The search process will be done from first element proceeding to the last element

*/

let firstIndex = countries.indexOf('PH');
console.log('Result of index method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of index method: ' + invalidCountry);


console.log('-----------------------------------------------------------------------');

// lastIndexOf()

/* 

    - Returns the index number of the last matching element found in an array
    - The search process will be done from last element proceeding to the first elements

*/

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 3);
console.log('Result of lastIndexOf method: ' + lastIndexStart);


console.log('-----------------------------------------------------------------------');


// slice()

/* 

    - Portions/Slice elements from an array AND returns a new array

*/

let slicedArrayA = countries.slice(2);
console.log('Result of slice method: ');
console.log(slicedArrayA);
console.log(countries);

let slicedArrayB = countries.slice(2, 4);
console.log('Result of slice method: ');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result of slice method: ');
console.log(slicedArrayC);


console.log('-----------------------------------------------------------------------');


// toString()

/* 

    - Returns an array as a string separated by commas

*/

let stringArray = countries.toString();
console.log('Result from to string method:');
console.log(stringArray);


console.log('-----------------------------------------------------------------------');


// concat()

/* 

    - Combines two arrays and returns the combined result

*/

let taskArrayA = ['drink HTML', 'eat JavaScript'];
let taskArrayB = ['inhale css', 'breath sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from to concat method:');
console.log(tasks);

// Combining multiple arrays 

console.log('Result from concat method:');
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTasks);

// Combining arrays with elements

let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log('Result from concat method');
console.log(combinedTasks);


console.log('-----------------------------------------------------------------------');


// join()

/* 

    - Returns an array as a string separated by specified separator string 

*/

let users = ['john', 'jane', 'joe', 'robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));


console.log('-----------------------------------------------------------------------');


// Iteration methods

/* 

    - Iterations= methods are loops designed to perform repetitive tasks on arrays
    - Iteration methods loops over all items in an array
    - Useful for manipulating array data resulting in complex tasks
    - Array Iteration methods normally work with a function supplied as an argument
    - How these works is by performing tasks that are pre-defined within an array's method

*/

// forEach()

/* 

    - Similar to a for loop that iterates on each array element
    - For each item in the array, the anonymous function passed in the forEach() method will be run
    - The Anonnymous function is able to recieve the current item being iterated or loop over by assigning a parameter
    - It's common practice to use the singular form of the array content for parameter names used in array loops

*/

allTasks.forEach(function(task) {
    console.log(task);
})

// Using forEach with conditional statements 
let filteredTasks = [];
allTasks.forEach(function(task){
    if(task.length > 10) {
        filteredTasks.push(task);
    }
})

console.log("Result of filtered tasks: ");
console.log(filteredTasks);


console.log('-----------------------------------------------------------------------');


//  map()

/* 

     - Iterates on each element AND returns new array with different values depending on the result of the functions operations
     - This is useful for performing tasks where mutating/changin the elements are required
     - Unlike the forEach method, the map method requires the use of a 'return
    statement in order to create another array with the performed operation

*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(numbers) {
    return numbers * numbers;
})

console.log("Original Array:")
console.log(numbers);
console.log("result of map method:");
console.log(numberMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function(numbers) {
    return numbers + numbers;
})

console.log(numberForEach);

// forEach(), loops over all items in the array as does map() but forEach() does not return a new array

console.log('-----------------------------------------------------------------------');

// every()
/* 

    -Checks if all elements in an array meet the given condition
    -This is useful for validating data stored in arrays
    especially when dealing with large amounts of data
    -Returns a true value if all elements meet the
    condition and false if otherwise

*/

let allValid = numbers.every(function(numbers) {
    return (numbers < 3);
});

console.log("Result of every method");
console.log(allValid);

let allValid2 = numbers.every(function(numbers) {
    return (numbers < 10);
});

console.log("Result of every method");
console.log(allValid2);


console.log('-----------------------------------------------------------------------');


// some()
/* 

    -Checks if at least one element in the array meets
    the given condition
    -Returns a true value if at least one lement meets
    the condition and false if otherwise

*/

let someValid = numbers.some(function(numbers) {
    return (numbers < 2);
})

console.log("Result of some method:");
console.log(someValid);


console.log('-----------------------------------------------------------------------');


// filter()

/* 

    - Returns new array that contains elements which meets the given condition
    - Returns and empty array if no elements were found
    - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
    - Mastery of loops can help us work effectively by reducing the amount of code we use

*/

let filterValid = numbers.filter(function(numbers) {
    return (numbers < 3);
});

console.log("Result of filter method:");
console.log(filterValid);

//  no element found

let nothingFound = numbers.filter(function(numbers) {
    return (numbers = 0);
});

console.log("Result of filter method:");
console.log(nothingFound);

// Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(numbers) {
    if(numbers < 3) {
        filteredNumbers.push(numbers);
    }
})

console.log("Result of filter method:");
console.log(filteredNumbers);


console.log('-----------------------------------------------------------------------');


// includes()

/* 

    -includes() method checks if the argument passed can be found in the array
    -it returns a boolean which can be saved in a variable
        - returns true if the argument is found in the
        array
        - returns false if it is not

*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");

console.log(productFound1);

let productFound2 = products.includes("Headset");

console.log(productFound2);

/* 

    -Methods can be chained using them one after another
    -The result of the first method is used on the second
    method untill all "Chained" rBth0ds have been resolved

*/

let filteredProducts = products.filter(function(products) {
    return products.toLowerCase().includes('a');
})

console.log(filteredProducts);


console.log('-----------------------------------------------------------------------');


// reduce()

/* 

    - Evaluates elements from left to right and returns/reduces the array into a single value

*/

let iteration = 0;

console.log(numbers);

let reducedArray = numbers.reduce(function(x,y) {
    // used to track the current iteration count and accumulator/currentValue data
    console.warn('Current iteration: ' + ++iteration);
    console.log('accumulator: ' + x);
    console.log('currentValue: ' + y);

    // the operation to reduce the array into a single value
    return x + y;
})

console.log("Result of reduced method: " + reducedArray);

let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x,y) {
    return x + ' ' + y
})

console.log("Result of reduced method: " + reducedJoin);