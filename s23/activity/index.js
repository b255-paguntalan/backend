// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

function trainer(name) {
    this.name = name;
    this.age = 10;
    this.friends = {hoenn: ["May", "max"], kanto: ["Brock", "Misty"]};
    this.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];

    this.talk = function() {
        console.log("Pikachu! I choose you!");
    }
    
}

let ash = new trainer('Ash Ketchum');
console.log(ash);

console.log("Result of dot notation:")
console.log(ash.name);

console.log("Result of square bracket notation:")
console.log(ash["pokemon"]);

console.log("Result of talk method:")
ash.talk();


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;

    this.tackle = function(enemy) {

        console.log(this.name+ " tackled " +enemy.name);
        console.log(enemy.name+ "'s health is now reduced to " +(enemy.health -= this.attack));

        if(enemy.health <= 0) {
            console.log(enemy.name+ " fainted");
        }
        console.log(enemy);
    }
}

let pikachu = new Pokemon ("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon ("Geodude", 8);
console.log(geodude);

let meowtwo = new Pokemon ("Meowtwo", 100);
console.log(meowtwo);

geodude.tackle(pikachu);

meowtwo.tackle(geodude);



//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}
