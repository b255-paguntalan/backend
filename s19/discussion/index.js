// Conditional Statements allows us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise

// [SECTION] if, else if, else statement

let numA = -1;

if(numA < 0) {
    console.log('Hello');
};

// The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

// You can check the condition. The expression results to a boolean true because of the use of the less than operator.

console.log(numA<0); 

// Results to true and som the if statement was run.

numA = 0;

if(numA < 0) {
    console.log("Hello again if numA is 0!");
};

// It will not run because the expression now results to false. 

let city = "New York"

if(city === "New York") {
    console.log("Welcome to New York City!");
};

// Else is clause

/* 

    - Executes a statement if previous conditions
    are false and if the specified conditions is
    true

    - The "else if" clause is optional and can be
    added to capture additional conditions to
    change the flow of a program


*/

let numH = 1;

if(numA < 0) {
    console.log('Hello');
}
else if (numH > 0) {
    console.log("World");
};

// We are able to run the else if() after we evaluated that the if condition was failed

// If the if() condition was passed and run, we will no longer evaluate the else if() and end the process there.

numA = 1;
if(numA > 0) {
    console.log('Hello');
}
else if(numH > 0) {
    console.log('World');
};

// else if() statement no longer ran because the if statement

city = "Tokyo";

if(city === "New York") {
    console.log("Welcome to New York City");
}
else if(city === "Tokyo") {
    console.log("Welcome to Tokyo, Japan!");
};

// Since we failed the condition for the first if(), we went to the else if() and checked and passed that condition

// Else statement

/* 

    -Executes a statement if all other conditions
    are false

    - The "else" statement is optional and can be
    added to capture any other result to change the
    flow of the program

*/

if(numA > 0) {
    console.log('Hello');
}
else if(numH === 0) {
    console.log("World");
}
else {
    console.log("Again");
};

// Else statements should only be added if there is a preceeding if condition. Else statements by itself will not work, however, if statements will work even if there is no else statement

// If, else if and else statements with functions

/* 

    - host of the time we would like to use If,
    else if and else statements with functions to
    control the flow of our application
    -gy including them inside functions, we can
    decide when certain conditions will be checked
    instead of exectuting statements when the
    JavaScript loads
    - The return statement can be utilized with
    contidional statements in combination with
    functions to change values to be used for other
    features

*/

function determineTyphoonIntensity(windspeed) {
    if(windspeed < 30) {
        return "Not a Typhoon Yet";
    }
    else if(windspeed <= 61) {
        return "Tropical depression detected";
    }
    else if(windspeed >= 62 && windspeed <= 88) {
        return "Tropical storm detected";
    }
    else if(windspeed >= 89 || windspeed <= 117) {
        return "Severe tropical storm detected";
    }
    else {
        return "Typhoon detected";
    };
};

let message = determineTyphoonIntensity(110);
console.log(message);

// MINI-ACTIVITY
// Create a function wit han if else statement inside 
// The function should test for if a given number is a even number or an odd number
// Console.log the output if it is an even number
// Console.log the output if it is an odd number

function evenOdd(number) {
    result = (number % 2);
    if(result === 0) {
        return "Even number";
    }
    else {
        return "Odd number";
    };
};

let answer = evenOdd(23);
console.log(answer);

// MINI-ACTIVITY END

// Truthy Examples

/* 

    - Tf the result of an expression in a condition
    results to a truthy value, the condition retunrs
    true and the corresponding statements are
    executed
    - Expressions are any unit of code that can be
    evaluated to a value

*/

if(true) {
    console.log("Truthy");
}
if(1) {
    console.log("Truthy");
}
if([]) {
    console.log("Truthy");
}

// Falsy examples
if(false) {
    console.log("Falsy");
}

if(false) {
    console.log("Truthy");
}
if(0) {
    console.log("Truthy");
}
if(undefined) {
    console.log("Truthy");
}

// [SECTION] Conditional (ternary) Operator

/* 

    - The conditional (Ternary) Operator takes in
    three operands
    1 , condition
    2. expression to execute if the condition is
    t ruthy
    3. expression to execute if the condition Is
    falsy
    - Can be used as an alternative to an "if else"
    statement
    - Commonly used for single statement execution
    where the result consists of only one line of
    code

    Syntax:
    (Expression) ? ifTrue : ifFalse;


*/

// Single Statement Execution

let ternaryResult = (1<18) ? true : false;
console.log("Result of Ternary operator: " + ternaryResult);

// Multi statement execution

let name;

function ifOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the legal age limit';
}

/* 

    - Input recieved from the prompt function is returned as a string data type
    -The "parseInt" function converts the input recieved into a number data type.

*/

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age > 18) ? ifOfLegalAge() : isUnderAge();

console.log("Result of Ternary Operator in fuctions: " + legalAge + ', ' + name)

// [SECTION] Switch Statements

/* 

    - The switch statement evaluates an expression
    and matches the expressions value to a case
    clause. The switch will then exectur•e the
    statements associated with that case, as well as
    statements in cases that follou the match

    - Switch cases are considered as "loops" meaning
    it will compare the expression with each of the
    case values until a match Is found

    - the "break" statement is used to terminate the
    current loop once a match has been found


*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {

    case 'monday':
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input valid day");
        break;
};

// [SECTION] Try-catch-finally statement

/* 

    - "try catch" statements are cc«nonly used
    handling
    -There are instances when the application
    error/warning that is not necessariliy an
    the context of our code
    for error
    returns an
    error xn
    -These errors are a result of an attempt of the
    programång language to help developers in creating
    efficient code.
    -They are used to specify response whenever en
    exception/error is recieved

*/

function showIntensityAlert(windspeed) {
    try{
        alert(determineTyphoonIntensity(windspeed));
    }

    // error/err are commonly used variable names used by developers for storing errors

    catch (error) {

        // The typeof operator is used to check the data type of a value "expression and returns a value uf Khat the type is string

        console.log(typeof error);

        // catch errors within the try statement
        // In this case the error is an unknown function alerat' which does not exist in JavaScript
        // "error .message" is used to access the information relating to an error object

        console.warn(error.message);
    }
    finally {

        // Continue execution Of code regardless Of success and failure of code execut on in the 'try' block to hance 'resolve error

        alert("Intensity updates will show new alert");
    }
}

showIntensityAlert(56);