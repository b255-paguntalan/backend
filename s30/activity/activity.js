// Activity #2
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "fruitsOnSale"}
]);

// Activity #3
db.fruits.aggregate([
    {$match: {stocks: {$gte:20}}},
    {$count: "enoughStock"}
]);

// Activity #4
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id" , avg_price: {$avg:"$price"}}}
])

// Activity #5
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id" , max_price: {$max:"$price"}}}
])

// Activity #6
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id" , min_price: {$min:"$price"}}}
])