// [SECTION] Syntax, Statements and Coments
// Statements in programming are instructions that we tell the computer to perform
// JS Statements useually end with the semicolon (;)
console.log("Hello World!");

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

// There are two types of comments:
// 1. The single line comment denoted by two slashes.
// 2. The multi-line comments denoted by a slash and an asterisk

// [SECTION] Variables

// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"

// Declaring variables
// Declaring variables - tells our device that a variable name is created and ready to store data
// Declaring a variable without assigning a value will automatically give it the value of undefined
let myVariable;
console.log(myVariable);
// console.lol() is usefull for printing out values of variables or certain results of code into the console.

let hello;
console.log(hello);
// Variables must be declared first before they are used 
// Using variables before they are declared will return an error.

// Guides in writting variables:
// 1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment opperator (=) to assign a value 
// 2. Variable names should start with a lowercase character, use camelCase for multiple words
// 3. For constant variables, use the 'const' keyword
// 4. Variable names should be indicative (or descriptive) of the value stored to avoid confusion

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given it's initial/starting value

let productName = 'desktop computer';
console.log(productName)

let productPrice = 18999;
console.log(productPrice);
// In the context of certain applications some variables information are constant and should not be changed 
// The interest rate for a loan savings account or a mortgage must not be changed due to real world concerns
const interest = 3.539;

// Reassigning variables values
// Reassigning a variable means changing it's inital or previous value into another value

productName = 'laptop';
console.log(productName);

// Reassigning variables vs. initializing variable
// Declares a variable first

let supplier;
// Initialization is done after a variable has been declared
// This is considered as initialization because it is the first time that a value has been assigned to a variable

supplier = "John Smith Tradings";
console.log(supplier);


// This is considered a reassignment because it's initial value was already declared 
supplier = "Zuitt Store";
console.log(supplier);

// var vs. let/const

    // var - is also used in declaring variables, but var is an ECMAScript (ES1) feature (JavaScript 1997)
    // let/const was introduced as a new feature in ES6(2015)

    // There are issues associated with variables declared with var, regarding hoisting.
    // Hoisting is JavaScripts default behaviour of moving declarations to the top
    // In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting

    // for example:
    a = 5;
    console.log(a);
    var a;
    
    // In the above example, variable a is used before declaring it. and the program works and displays the output of 5

    // let/const local/global scope 
    // scope essencially means where these variables are available for use
    // let and const are block scope
    // A block is a chunk of code bounded by curly braces {} A block lives in curly braces. Anything within curly braces is a block.

// Multiple variable declarations
// Multiple variables may be declared in one line though it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let/const" keywords

let prodductCode = 'DC017', productBrand = "Dell";
console.log(prodductCode, productBrand);

// [SECTION] Data Types

// Strings
// Strings are  a series of characters that create a word, a phrase, a sentence, or anything related to creating text
// Strings in JavaScript can be written using either single ('') or double ("") quote

let country = 'Philippines';
let province = "Metro Manila";

// Concatinating string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escape character (\) in strings in combination with other characters can produce different effects
// The "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers 
// Integers/whole numbers

let headCount = 26;
console.log(headCount);

// Decimal number/fractions

let grade = 98.7;
console.log(grade)

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our applications respont to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log("isMarries: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays 
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Different data types 
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object

let person = {
    fullname: 'Juan Dela Cruz',
    age: 35,
    isMarried: false,
    contact: ["+63917 123 4567" , "8123 4567"],
    address: {
        houseNumber: '345',
        city: "Manila"
    }
}

console.log(person);

// They're also useful for creating abstract objects

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6
}

console.log(myGrades);

// typeof operator is used to determine the type of data or the value of a variable.
console.log(typeof myGrades);

const anime = ['one piece', 'one punch man', 'attack on titan'];
console.log(anime[0]);

anime[0] = ['kimetsu no yaiba'];
console.log(anime);

// Null
// It is used to intentionally express the absence of a value in a variable decalration/initialization

let spouse = null;
// Using null compared to a 0 value is much better for a readability purposes

// Null is also considered to be a data type of tis own compared to 0 which os a data type of number

// Undefined 
// Represents the state of a variable that has been declared but without assigned calue

let fullName;
console.log(fullName);

// Undefined vs. null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// Null means that a variable was created and was assigned a value that does not hold any amount














